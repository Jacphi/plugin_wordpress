<?php

/*
 Plugin Name: Cost Calculator
 Plugin URI: https://horygin.com/plugins/cost_calculator
 Description: A fast cost calculator !
 Version: 0.1
 Author: Horygin
 Author URI: https://horygin.com

 */

define("COST_CALCULATOR_PLUGIN_DIR", plugin_dir_path(__FILE__));

require_once COST_CALCULATOR_PLUGIN_DIR . 'includes/functions.php';

// add_action('admin_menu', 'add_admin_link');


// // Add a new top level menu link to the ACP
// function add_admin_link()
// {
//     add_menu_page(
//         'Cost calculator', // Title of the page
//         'Cost calculator', // Text to show on the menu link
//         'manage_options', // Capability requirement to see the link
//         'main_settings_page', // The 'slug' - file to display when clicking the link
//         'main_settings_page_function'
//     );
// }

// function main_settings_page_function()
// {
//     include_once plugin_dir_path(__FILE__) . 'includes/main_settings.php';
// }
