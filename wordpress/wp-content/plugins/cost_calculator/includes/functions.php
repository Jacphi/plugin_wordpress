<?php
/*
 * Add new menu to the Admin Control Panel
 */

// Hook the 'admin_menu' action hook, run the function named 'add_admin_link()'
add_action('admin_menu', 'add_admin_link');
add_shortcode('insert_cost_calculator', 'short_code_function');


// Add a new top level menu link to the ACP
function add_admin_link()
{
    add_menu_page(
        'Cost calculator', // Title of the page
        'Cost calculator', // Text to show on the menu link
        'manage_options', // Capability requirement to see the link
        'main_settings_page', // The 'slug' - file to display when clicking the link
        'main_settings_page_function'
    );
}

function main_settings_page_function()
{
    include_once COST_CALCULATOR_PLUGIN_DIR . 'includes/main_settings.php';
}

// Add shortcode
function short_code_function()
{
    ob_start();


    include_once COST_CALCULATOR_PLUGIN_DIR . 'assets/index.php';
    // include_once COST_CALCULATOR_PLUGIN_DIR . 'includes/shortcode_form.php';

    return ob_get_clean();
    // return "<p>Shortcode test 2</p>";
}
