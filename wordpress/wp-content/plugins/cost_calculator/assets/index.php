<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <?php
  wp_register_style('style.css', plugin_dir_url(__FILE__) . 'style.css');
  wp_enqueue_style('style.css');
  ?>
  <!-- <link rel="stylesheet" href="assets/style.css" /> -->
  <title>XOS Calculateur</title>
</head>

<body>
  <div class="Container">
    <form action="" method="post">
      <div class="Content">
        <?php
        if (!isset($_POST["first_njj"])) {
        ?>
          <div class="Form-Section-1">
            <!-- Section 1 taille de l'entreprise -->
            <div class="Taille">
              <div>
                <p class="Taille-Titre">
                  1. Taille de l'entreprise (dont éventuelle(s) filiale(s))
                </p>
                <div class="range-slide">
                  <div class="label-range">
                    <p>-500</p>
                    <p>500-5000</p>
                    <p>+5000</p>
                  </div>
                  <input type="range" min="0" max="500" name="" value="100" step="" id="myrange" class="range" />
                </div>
              </div>
            </div>
            <!-- Section 2 de personnes à former -->
            <div class="Nbre-personnes">
              <p class="Taille-Titre">2. Nombre de personne à former</p>
            </div>
            <!-- Icone button -->
            <div class="espace-button">
              <!-- Button 1 -->
              <div class="button-list1">
                <div class="button-select">
                  <div class="chekbox">
                    <!-- <input type="checkbox" name="" id="check" /> -->
                    <label class="check" for=""></label>
                  </div>
                  <img src="/icon/employee.png" alt="" class="icon" />
                  <p class="btn-text">20-50</p>
                </div>
              </div>
              <!-- Button 2 -->
              <div class="button-list1">
                <div class="button-select">
                  <div class="chekbox">
                    <!-- <input type="checkbox" name="" id="check" /> -->
                    <label class="check" for=""></label>
                  </div>
                  <img src="/icon/employee.png" alt="" class="icon" />
                  <p class="btn-text">51-100</p>
                </div>
              </div>
              <!-- Button 3 -->
              <div class="button-list1">
                <div class="button-select">
                  <div class="chekbox">
                    <!-- <input type="checkbox" name="" id="check" /> -->
                    <label class="check" for=""></label>
                  </div>
                  <img src="/icon/employee.png" alt="" class="icon" />
                  <p class="btn-text">51-100</p>
                </div>
              </div>
              <!-- Button 4 -->
              <div class="button-list1">
                <div class="button-select">
                  <div class="chekbox">
                    <!-- <input type="checkbox" name="" id="check" /> -->
                    <label class="check" for=""></label>
                  </div>
                  <img src="/icon/employee.png" alt="" class="icon" />
                  <p class="btn-text">51-100</p>
                </div>
              </div>
            </div>
            <div class="espace-button2">
              <!-- Button 1 -->
              <div class="button-list1">
                <div class="button-select">
                  <div class="chekbox">
                    <!-- <input type="checkbox" name="" id="check" /> -->
                    <label class="check" for=""></label>
                  </div>
                  <img src="/icon/employee.png" alt="" class="icon" />
                  <p class="btn-text">20-50</p>
                </div>
              </div>
              <!-- Button 2 -->
              <div class="button-list1">
                <div class="button-select">
                  <div class="chekbox">
                    <!-- <input type="checkbox" name="" id="check" /> -->
                    <label class="check" for=""></label>
                  </div>
                  <img src="/icon/employee.png" alt="" class="icon" />
                  <p class="btn-text">51-100</p>
                </div>
              </div>
              <!-- Button 3 -->
              <div class="button-list1">
                <div class="button-select">
                  <div class="chekbox">
                    <!-- <input type="checkbox" name="" id="check" /> -->
                    <label class="check" for=""></label>
                  </div>
                  <img src="/icon/employee.png" alt="" class="icon" />
                  <p class="btn-text">51-100</p>
                </div>
              </div>
              <div>
                <div>
                  <label for="first">Input field 1</label>
                  <input type="text" name="first_njj" id="first">
                </div>
                <div>
                  <label for="second">Input field 2</label>
                  <input type="text" name="second_njj" id="second">
                </div>
                <div>
                  <!-- <input name="submit" type="submit" value="Submit"> -->
                  <!-- <button id="njj_btn" name="submit">Submit</button> -->
                </div>
              </div>
            </div>
          </div>
          <div class="Form-Section-2">
            <!-- Titre -->
            <p class="Taille-Titre">Vos Coordonnées</p>
            <div class="Formulaire">
              <form action="">
                <div><label for="Nom"> Votre Nom</label></div>
                <div><input type="text" id="Nom" /></div>
                <div><label for="Prénom"> Votre prénom</label></div>
                <div><input type="text" id="Prénom" /></div>
                <div><label for="Email"> Votre adresse mail</label></div>
                <div><input type="text" id="Email" /></div>
                <div><label for="Num"> Votre numéro</label></div>
                <div><input type="text" id="Num" /></div>
              </form>
            </div>
          </div>
          <div class="Text-Section">
            <div class="Text-Content">
              <span class="Text-1">Renseignez le calculateur à gauche pour recevoir votre devis
                personnalisé.</span>
              <p class="Text-2">
                <span class="Text-Calcul"> Calculez le prix</span> de la solution
                e-learning
                <span class="Text-Color">adaptée à votre structure</span>.
              </p>
              <p class="btn-1">
                JE DÉCOUVRE MON <br />
                OFFRE ADAPTÉE
              </p>
            </div>
            <div class="Text-Content-2">
              <span class="Text-1">Renseignez le formulaire à gauche pour découvrir votre devis
                personnalisé.</span>
              <p class="Text-2">
                Recevez une solution
                <span class="Text-Color">personnalisée et adaptée</span> à votre
                structure.
              </p>
              <button type="submit" name="submit" class="btn-2">JE CALCULE MON DEVIS</button>
            </div>
          </div>
        <?php
        } else {
        ?>
          <div class="step3">
            <iframe name="content" frameborder="0" width="1000" height="500" scrolling="no" style="display: none;">
              <?php

              // require_once COST_CALCULATOR_PLUGIN_DIR . 'includes/functions.php';

              if (isset($_POST["submit"])) {
                echo $_POST["first_njj"];
                echo $_POST["second_njj"];
                var_dump("Hello world");

                $user_name = "Username" . $_POST["first_njj"];
                $user_email = $_POST["second_njj"] . "@gmail.com";

                // check if the username is taken
                $user_id = username_exists($user_name);


                // check that the email address does not belong to a registered user
                if (!$user_id && email_exists($user_email) === false) {
                  // create a random password
                  $random_password = wp_generate_password(12, false);
                  // create the user
                  $user_id = wp_create_user(
                    $user_name,
                    $random_password,
                    $user_email
                  );
                }

                var_dump($random_password);
              } ?>
            </iframe>
            <p>
              <?= $user_name; ?>
              <?= $user_email; ?>
            </p>
          </div>
        <?php
        }
        ?>
      </div>
    </form>
  </div>
  <?php
  wp_register_script('script.js', plugin_dir_url(__FILE__) . 'index.js');
  wp_enqueue_script("script.js");
  ?>
  <!-- <script src="/index.js"></script> -->
</body>

</html>