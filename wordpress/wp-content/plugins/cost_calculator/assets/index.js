window.onload = (e) => {
  //je stocke
  const form1 = document.querySelector(".Form-Section-1");
  console.log(form1);
  const form2 = document.querySelector(".Form-Section-2");
  console.log(form2);

  const step3 = document.querySelector(".step3");
  const TextSection = document.querySelector(".Text-Section");

  const TextZone1 = document.querySelector(".Text-Content");
  console.log(TextZone1);
  const TextZone2 = document.querySelector(".Text-Content-2");
  console.log(TextZone2);
  const buton1 = document.querySelector(".btn-1");
  console.log(buton1);
  const buton2 = document.querySelector(".btn-2");

  //Je soumet a une action

  buton1.addEventListener("click", function () {
    console.log("Buton Cliquer");
    form1.classList.toggle("Form-Section-Off");
    form2.classList.toggle("Form-Section-2-On");
    TextZone1.classList.toggle("Text-Content-Off");
    TextZone2.classList.toggle("Text-Content-2-On");
  });

  buton2.addEventListener("click", function (e) {
    console.log("Buton Cliquer");
    step3.classList.add("step3-on");

    // form2.classList.remove("Form-Section-2-On");
    form2.classList.add("Form-Section-2-Off");

    // TextZone2.classList.remove("Text-Content-2-On");
    TextZone2.classList.add("Text-Content-2-Off");

    // TextSection.classList.remove("Text-Content-2-Off");
    TextSection.classList.add("Text-Section-Off");
  });
};
