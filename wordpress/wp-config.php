<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cost_calculator');

/** Database username */
define('DB_USER', 'root');

/** Database password */
define('DB_PASSWORD', '1999');

/** Database hostname */
define('DB_HOST', 'localhost');

/** Database charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The database collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'p5FU)vKsCM?)D[QI8/.jcJ~WiFjfD:NkA2ub4]S:j9<U7i`v0JXSL[0s-!Ux<rk^');
define('SECURE_AUTH_KEY',  'O!o~v=z/teue.q,(/jg/sDaN2QJKN|ad| I<.#j0dNwteylU]t6@[{-`>wV/q+;H');
define('LOGGED_IN_KEY',    '5)yI^h`hnp94r=)Z;)eMApS30O4HlEBr6Cw6N#FT:9|%cTFB({cp<aixtfA2efWz');
define('NONCE_KEY',        '9k#F$&J6l%~_Epc8Zi:glgj= Hmglfc0Ah=uv4G*BCRzYlsazonpo7s[%DFE{L[]');
define('AUTH_SALT',        '*AHYr59M6^x|][aV`t/N!c<5%%rE~KNDO:C2v%TP8_Qmr kW$o-:!DHJ!aiA[bVw');
define('SECURE_AUTH_SALT', '.u3)Qr4-:Z<jl0hgi5GW],.;B~S+sFF@+` vH.Qh69+{kO&e;2!Xe[={?6uuG&Ip');
define('LOGGED_IN_SALT',   'Ep<cEzxB6a?l;(nwr6dBCGHXqW.[|gX=[5*kxrrZd[5Q2OqT0CI6_w|5ugn^&`8y');
define('NONCE_SALT',       'i..=!=kQ*[D.`gW*.!,yNB:m){1[TV-6^cL4Qpt~ #nb{4lL2S W5W@G9xDC;hY ');

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', true);

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
	define('ABSPATH', __DIR__ . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
